import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { RightsComponent } from "./rights/rights.component";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    children: [{ path: "rights", component: RightsComponent }],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {}
