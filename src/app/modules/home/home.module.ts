import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { ContactusComponent } from './contactus/contactus.component';
import { LayoutComponent } from './layout/layout.component';


@NgModule({
  declarations: [
    ContactusComponent,
    LayoutComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
